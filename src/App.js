import React, { useState, useEffect } from "react";
import { Spinner, Col, Row } from "react-bootstrap";

import Header from "./components/header/Header";
import SearchPanel from "./components/SearchPanel";
import ContactBox from "./components/contact-box/ContactBox";

function App() {
  const [contacts, setContacts] = useState([]);
  const [filteredContacts, setFilteredContacts] = useState([]);
  const [displayFiltered, setDisplayFiltered] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      );
      const data = await response.json();
      const sortedData = data.sort((a, b) =>
        a.last_name > b.last_name ? 1 : -1
      );
      setContacts(sortedData);
    };

    fetchData();
  }, []);

  const filterHandler = (event) => {
    const {
      target: { value },
    } = event;
    setDisplayFiltered(!!value);
    const filteredData = contacts.filter(
      (item) =>
        item.first_name.toLowerCase().includes(value.toLowerCase()) ||
        item.last_name.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredContacts(filteredData);
  };

  return (
    <div className="App">
      <Header />
      <SearchPanel filterHandler={filterHandler} />
      {contacts.length ? (
        <ContactBox data={displayFiltered ? filteredContacts : contacts} />
      ) : (
        <Row>
          <Col className="d-flex justify-content-center">
            <Spinner animation="grow" />
          </Col>
        </Row>
      )}
    </div>
  );
}

export default App;
