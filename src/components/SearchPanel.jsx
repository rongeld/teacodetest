import React from "react";
import { InputGroup, FormControl } from "react-bootstrap";
import { BsSearch } from "react-icons/bs";

const SearchPanel = ({ filterHandler }) => (
  <InputGroup>
    <InputGroup.Prepend>
      <InputGroup.Text id="basic-addon1">
        <BsSearch />
      </InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl
      placeholder="Search..."
      aria-label="Search"
      onChange={(e) => filterHandler(e)}
    />
  </InputGroup>
);

export default SearchPanel;
