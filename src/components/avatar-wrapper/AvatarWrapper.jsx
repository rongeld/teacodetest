import React from "react";
import "./AvatarWrapperStyles.css";

const AvatarWrapper = ({ url }) => (
  <div className="img-wrapper">
    <img src={url} alt="img" />
  </div>
);

export default AvatarWrapper;
