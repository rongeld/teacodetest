import React from "react";
import { Container } from "react-bootstrap";
import ContactItem from "../contact-item/ContactItem";

import "./ContactBoxStyles.css";

const ContactBox = ({ data }) => (
  <div className="pt-3 contacts-wrapper">
    <Container>
      {data.map((item) => (
        <ContactItem key={item.id} {...item} />
      ))}
    </Container>
  </div>
);

export default ContactBox;
