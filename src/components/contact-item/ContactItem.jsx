import React, { useState } from "react";
import { Row, Col, Form } from "react-bootstrap";

import AvatarWrapper from "../avatar-wrapper/AvatarWrapper";

const ContactItem = ({ avatar, first_name, last_name, email, id }) => {
  const [isChecked, setIsChecked] = useState(false);
  const checkedHandler = () => {
    if (!isChecked) console.log(id);
    setIsChecked((prevState) => !prevState);
  };

  return (
    <Row
      className="pl-3 mb-3"
      style={{ borderBottom: "1px solid #d8d8d8", cursor: "pointer" }}
      onClick={checkedHandler}
    >
      <AvatarWrapper url={avatar} />
      <Col>
        <span
          style={{ fontWeight: "bolder" }}
        >{`${first_name} ${last_name}`}</span>
        <p className="mb-0">{email}</p>
      </Col>
      <Col>
        <Form.Check type="checkbox" readOnly checked={isChecked} />
      </Col>
    </Row>
  );
};

export default ContactItem;
