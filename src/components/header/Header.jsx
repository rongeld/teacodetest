import React from "react";
import "./HeaderStyles.css";

const Header = () => (
  <header>
    <h1>Contacts</h1>
  </header>
);

export default Header;
